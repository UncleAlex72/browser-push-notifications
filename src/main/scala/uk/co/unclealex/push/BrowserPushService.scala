package uk.co.unclealex.push

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import nl.martijndwars.webpush.PushAsyncService
import org.bouncycastle.jce.provider.BouncyCastleProvider
import uk.co.unclealex.mongodb.dao.Database.Database

import java.security.Security
import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

/** Send call notifications to browsers.
  */
trait BrowserPushService:

  /** The VAPID public key.
    * @return
    */
  def publicKey(): String

  /** Subscribe to a push subscription.
    * @param pushSubscription
    *   The push subscription to subscribe to.
    * @return
    */
  def subscribe(
      pushSubscription: PushSubscription
  ): Source[PushSubscription, NotUsed]

  /** Send a message to all browsers.
    * @param body
    *   The message to send.
    * @return
    */
  def notifyAll(message: String): Source[PushResponse, NotUsed]

  /** Send a message to all browsers owned by a user.
    * @param body
    *   The message to send.
    * @return
    */
  def notify(user: String, message: String): Source[PushResponse, NotUsed]

object BrowserPushService:

  def apply(
      eventualDatabase: Future[Database],
      browserPushConfiguration: BrowserPushConfiguration,
      clock: Clock
  )(using
      actorSystem: ActorSystem,
      ec: ExecutionContext
  ): BrowserPushService =

    Security.addProvider(new BouncyCastleProvider)

    val pushEndpointDao: PushEndpointDao =
      new MongoDbPushEndpointDao(eventualDatabase, clock)

    val pushService = new PushAsyncService(
      browserPushConfiguration.publicKey,
      browserPushConfiguration.privateKey,
      browserPushConfiguration.domain
    )

    new BrowserPushServiceImpl(
      browserPushConfiguration,
      pushEndpointDao,
      pushService
    )
