package uk.co.unclealex.push

import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.Source
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.MongoDbDao

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

/** The MongoDB backed implementation of [[PushEndpointDao]]
  *
  * @param reactiveMongoApi
  *   The underlying MongoDB API.
  * @param ec
  *   The execution context used to chain futures.
  */
class MongoDbPushEndpointDao(
    eventualDatabase: Future[Database],
    clock: Clock
)(using actorSystem: ActorSystem, ec: ExecutionContext)
    extends MongoDbDao[PushSubscription](
      eventualDatabase,
      clock,
      "push-subscriptions"
    )
    with PushEndpointDao:

  override def findAll(): Source[PushSubscription, NotUsed] =
    super[MongoDbDao].findAll()

  /** Find by user
    */
  override def find(user: String): Source[PushSubscription, NotUsed] =
    findOne("user.user" === user)

  override def insertOrUpdate(
      ps: PushSubscription
  ): Source[PushSubscription, NotUsed] =
    val changes: PushSubscription => PushSubscription =
      _.copy(endpoint = ps.endpoint, keys = ps.keys, user = ps.user)
    upsert(
      "endpoint" === ps.endpoint,
      changes
    )

  /** Remove a stale push subscription.
    *
    * @param endpoint
    *   The endpoint to remove.
    * @return
    */
  override def remove(endpoint: String): Source[Long, NotUsed] =
    removeOne("endpoint" === endpoint)
