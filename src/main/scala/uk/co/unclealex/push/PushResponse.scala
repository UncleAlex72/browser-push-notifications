package uk.co.unclealex.push

sealed trait PushResponse:
  val pushSubscription: PushSubscription

object PushResponse:

  case class Success(override val pushSubscription: PushSubscription)
      extends PushResponse

  case class Failure(
      override val pushSubscription: PushSubscription,
      t: Throwable
  ) extends PushResponse
