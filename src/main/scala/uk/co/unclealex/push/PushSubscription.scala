package uk.co.unclealex.push

import io.circe.Codec
import uk.co.unclealex.mongodb.bson.{BsonDocumentCodec, ID}
import uk.co.unclealex.mongodb.persistable.{Persistable, Upsertable}
import uk.co.unclealex.stringlike.StringLike.given
import uk.co.unclealex.stringlike.{StringLike, StringLikeJsonSupport}

import java.time.Instant

/** A push subscription received from a browser that can be used to send
  * notifications.
  * @param endpoint
  *   The endpoint that will receive notifications.
  * @param p256dh
  *   An Elliptic curve Diffie–Hellman public key on the P-256 curve.
  * @param auth
  *   An authentication secret.
  */
case class PushSubscription(
    _id: Option[ID],
    endpoint: String,
    keys: PushSubscription.Keys,
    user: PushSubscription.User,
    lastUpdated: Option[Instant],
    dateCreated: Option[Instant]
) derives Codec,
      Persistable

/** Convert a push subscription to and from JSON.
  */
object PushSubscription {

  case class Keys(p256dh: String, auth: String) derives Codec, BsonDocumentCodec
  case class User(user: Option[String], userAgent: Option[String])
      derives Codec,
        BsonDocumentCodec

  given Upsertable[PushSubscription] with
    override def zero: PushSubscription =
      PushSubscription(
        _id = None,
        endpoint = "",
        keys = PushSubscription.Keys(p256dh = "", auth = ""),
        user = PushSubscription.User(user = None, userAgent = None),
        lastUpdated = None,
        dateCreated = None
      )
}
