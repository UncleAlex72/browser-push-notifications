package uk.co.unclealex.push

import org.apache.pekko.{Done, NotUsed}
import org.apache.pekko.stream.scaladsl.Source

/** Persist browser push endpoints.
  */
trait PushEndpointDao:

  /** Remove a stale push subscription.
    * @param endpoint
    *   The endpoint to remove.
    * @return
    */
  def remove(endpoint: String): Source[Long, NotUsed]

  /** Add or update a push subscription.
    *
    * @param pushSubscription
    *   The subscription to add or insert.
    * @return
    */
  def insertOrUpdate(
      pushSubscription: PushSubscription
  ): Source[PushSubscription, NotUsed]

  /** Get all stored push subscriptions.
    * @return
    */
  def findAll(): Source[PushSubscription, NotUsed]

  /** Find by user
    */
  def find(user: String): Source[PushSubscription, NotUsed]
