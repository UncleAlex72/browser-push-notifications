package uk.co.unclealex.push

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Flow, Source}
import com.typesafe.scalalogging.StrictLogging
import nl.martijndwars.webpush.{Notification, PushAsyncService}

import java.security.Security
import scala.jdk.FutureConverters._

/** The default implementation of [[BrowserPushService]]
  * @param pushConfiguration
  *   The VAPID keys and domain required to send notifications to browsers.
  * @param pushEndpointDao
  *   The DAO used to persist subscriptions.
  */
class BrowserPushServiceImpl(
    pushConfiguration: BrowserPushConfiguration,
    pushEndpointDao: PushEndpointDao,
    pushService: PushAsyncService
) extends BrowserPushService
    with StrictLogging:

  override def publicKey(): String = pushConfiguration.publicKey

  override def subscribe(
      pushSubscription: PushSubscription
  ): Source[PushSubscription, NotUsed] =
    pushEndpointDao.insertOrUpdate(pushSubscription)

  /** Send a message to all browsers.
    *
    * @param body
    *   The body to send.
    * @return
    */
  override def notifyAll(message: String): Source[PushResponse, NotUsed] =
    pushNotifications(message, pushEndpointDao.findAll())

  /** Send a message to all browsers owned by a user.
    *
    * @param body
    *   The body to send.
    * @return
    */
  override def notify(
      user: String,
      message: String
  ): Source[PushResponse, NotUsed] =
    pushNotifications(message, pushEndpointDao.find(user))

  def pushNotifications(
      message: String,
      pushSubscriptions: Source[PushSubscription, NotUsed]
  ): Source[PushResponse, NotUsed] =
    pushSubscriptions.via(sendMessageFlow(message))

  def sendMessageFlow(
      message: String
  ): Flow[PushSubscription, PushResponse, NotUsed] =
    Flow[PushSubscription].flatMapConcat: pushSubscription =>
      val endpoint: String = pushSubscription.endpoint
      val notification =
        new Notification(
          endpoint,
          pushSubscription.keys.p256dh,
          pushSubscription.keys.auth,
          message
        )
      val httpSource: Source[PushResponse, NotUsed] =
        Source
          .future(pushService.send(notification).asScala)
          .map: httpResponse =>
            val statusCode = httpResponse.getStatusCode
            val statusText = httpResponse.getStatusText
            logger.debug(
              s"""Received response $statusCode: "$statusText" from endpoint $endpoint for message: "$message""""
            )
            if (statusCode >= 400)
              PushResponse.Failure(
                pushSubscription,
                new IllegalStateException(
                  s"""Received invalid status code $statusCode for message: "$message" """
                )
              )
            else
              PushResponse.Success(pushSubscription)

      val pushResponseSource: Source[PushResponse, NotUsed] =
        httpSource.recover { case t: Throwable =>
          PushResponse.Failure(pushSubscription, t)
        }
      pushResponseSource.via(removeFailures())

  def removeFailures(): Flow[PushResponse, PushResponse, NotUsed] =
    Flow[PushResponse].flatMapConcat {
      case pushResponse @ PushResponse.Success(_) => Source.single(pushResponse)
      case pushResponse @ PushResponse.Failure(pushSubscription, ex) =>
        def orUnknown(builder: PushSubscription => Option[String]): String =
          builder(pushSubscription).getOrElse("unknown")

        logger.error(
          s"""Sending message to user ${orUnknown(
              _.user.user
            )} at user agent ${orUnknown(_.user.userAgent)} failed""",
          ex
        )
        val endpoint: String = pushSubscription.endpoint
        pushEndpointDao.remove(endpoint).map(_ => pushResponse)
    }
