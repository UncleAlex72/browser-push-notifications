import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

organization := "uk.co.unclealex"
organizationName := "unclealex"
name := "browser-push-notifications"

val _scalaVersion = "3.6.2"
val mongoDbVersion = "5.0.1"
val circeVersion = "0.14.1"

scalaVersion := _scalaVersion

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  Resolver.jcenterRepo
)

libraryDependencies ++= Seq(
  //backend libraries
  "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
  "nl.martijndwars" % "web-push" % "5.1.1",
  "org.bouncycastle" % "bcprov-jdk15on" % "1.70"
) ++ Seq(
  // test
  "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion % "test"
)

fork in Test := false

pomIncludeRepository := { _ =>
  false
}

publishTo := sonatypePublishToBundle.value

publishMavenStyle := true

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("publishLocal"),
  releaseStepCommand("publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)

Global / onChangedBuildSource := ReloadOnSourceChanges
